<?php
namespace sms;
//实例化：$Sms = new \sms\Sms(); or use sms\Sms; new Sms();
// $Sms->sendSms("手机号");
use think\Session;
class Sms
{
	private $target = "http://106.ihuyi.cn/webservice/sms.php?method=Submit";

	public function sendSms($mobile = "")
	{
		if(empty($mobile)){
			throw new \think\Exception('手机号码不能为空');
		}

		$config = current(get_app_config('sms'));
		if(!$config){
			throw new \think\Exception('没有获取到短信配置');
		}
		$length = intval($config['smsLength']);
		$code = $this->random($length,1);

		$post = "account=".$config['smsUser']."&password=".$config['smsPass']."&mobile=".$mobile."&content=".rawurlencode("您的验证码是：".$code."。请不要把验证码泄露给其他人。");
		$result =  $this->xml_to_array($this->curl_post($post, $this->target));
		if($result['SubmitResult']['code'] == 2){
			Session::init([
				'expire' => 3600
			]);
			Session::set('mobile',$mobile);
			Session::set('mobile_code',$code);
			return true;
		}else{
			return false;
		}
	}

	//post请求
	public function curl_post($post,$url)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
		$return_str = curl_exec($curl);
		curl_close($curl);
		return $return_str;
	}

	//将xml数据转换为数组格式。
	public function xml_to_array($xml){
		$reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
		if(preg_match_all($reg, $xml, $matches)){
			$count = count($matches[0]);
			for($i = 0; $i < $count; $i++){
			$subxml= $matches[2][$i];
			$key = $matches[1][$i];
				if(preg_match( $reg, $subxml )){
					$arr[$key] = $this->xml_to_array( $subxml );
				}else{
					$arr[$key] = $subxml;
				}
			}
		}
		return $arr;
	}

	//返回随机整数。
	public function random($length = 6 , $numeric = 0) {
		PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
		if($numeric) {
			$hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
		} else {
			$hash = '';
			$chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
			$max = strlen($chars) - 1;
			for($i = 0; $i < $length; $i++) {
				$hash .= $chars[mt_rand(0, $max)];
			}
		}
		return $hash;
	}


}

?>
