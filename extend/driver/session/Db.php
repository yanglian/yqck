<?php
namespace driver\session;

use SessionHandler;

/**
 * session配置 'type' => '\driver\session\Db'
 * 数据库方式Session驱动
 */

class Db extends SessionHandler
{
    protected $config  = [
        'expire'     => 0, // Session有效期
        'prefix'     => 'think', // Session前缀
        'table_name' => 'session' // 表名（不包含前缀）
    ];

    public function __construct($config = [])
    {
        $this->config = array_merge($this->config, $config);
    }

    /**
     * 打开Session
     * @access public
     * @param string $save_path
     * @param mixed $session_name
     * @return bool
     */
    public function open($save_path, $session_name)
    {   
        return true;
    }

    /**
     * 关闭Session
     * @access public
     */
    public function close()
    {
        $this->gc(ini_get('session.gc_maxlifetime'));
        return true;
    }

    /**
     * 读取Session
     * @access public
     * @param string $session_id
     * @return bool|string
     */
    public function read($session_id)
    {
        $map['session_id'] = ['eq', $this->config['prefix'] . $session_id];
        if ($this->config['expire'] != 0) {
            $map['update_time'] = ['gt', time() - $this->config['expire']];
        }
        return \think\Db::name($this->config['table_name'])->where($map)->value('data');
    }

    /**
     * 写入Session
     * @access public
     * @param string $session_id
     * @param String $session_data
     * @return bool
     */
    public function write($session_id, $session_data)
    {
        $result = \think\Db::name($this->config['table_name'])->where('session_id', $this->config['prefix'] . $session_id)->find();
        $data   = ['session_id' => $this->config['prefix'] . $session_id, 'update_time' => time(), 'data' => $session_data];
        if ($result) {
            $affect_rows = \think\Db::name($this->config['table_name'])->update($data);
        } else {
            $affect_rows = \think\Db::name($this->config['table_name'])->insert($data);
        }

        return $affect_rows ? true : false;
    }

    /**
     * 删除Session
     * @access public
     * @param string $session_id
     * @return bool
     */
    public function destroy($session_id)
    {
        $result = \think\Db::name($this->config['table_name'])->where('session_id', $this->config['prefix'] . $session_id)->delete();
        return $result ? true : false;
    }

    /**
     * Session 垃圾回收
     * @access public
     * @param string $sessMaxLifeTime
     * @return bool
     */
    public function gc($sessMaxLifeTime)
    {
        if ($this->config['expire'] != 0) {
            $map['update_time'] = ['<', time() - $this->config['expire']];
        } else {
            $map['update_time'] = ['<', time() - $sessMaxLifeTime];
        }
        $result = \think\Db::name($this->config['table_name'])->where($map)->delete();
        return $result ? true : false;
    }

}
