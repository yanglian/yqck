/* This file is created by mysql backup 2017-07-29 11:23:39 */
 /* 创建表结构 `yl_active_record` */
 DROP TABLE IF EXISTS `yl_active_record`;/* Mysql backup Separation */ CREATE TABLE `yl_active_record` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 主账户 2子账户',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '激活数量',
  `invite` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '邀请人奖励',
  `charity` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '慈善基金',
  `abonus` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '分红基金',
  `shopping` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '购物币',
  `time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 创建表结构 `yl_admin` */
 DROP TABLE IF EXISTS `yl_admin`;/* Mysql backup Separation */ CREATE TABLE `yl_admin` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `mobile` char(11) NOT NULL COMMENT '手机号',
  `nickname` varchar(20) NOT NULL COMMENT '昵称',
  `pass` char(32) NOT NULL COMMENT '操作密码',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_admin` */
 INSERT INTO `yl_admin` VALUES ('1','18627565346','技术1','4297f44b13955235245b2497399d7a93','1'),('2','13226033006','技术2','4297f44b13955235245b2497399d7a93','1');/* Mysql backup Separation */
 /* 创建表结构 `yl_admin_log` */
 DROP TABLE IF EXISTS `yl_admin_log`;/* Mysql backup Separation */ CREATE TABLE `yl_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `content` text NOT NULL COMMENT '日志内容',
  `create_time` int(11) NOT NULL COMMENT '记录时间',
  `create_ip` char(15) NOT NULL COMMENT '记录ip',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_admin_log` */
 INSERT INTO `yl_admin_log` VALUES ('2','管理员：18627565346 昵称：技术请求登录，验证码：08346','1482308112','127.0.0.1'),('3','管理员：18627565346 昵称：技术成功登录后台。','1482308142','127.0.0.1'),('7','管理员：18627565346 昵称：技术请求登录，验证码：56947','1482309912','127.0.0.1'),('8','管理员：18627565346 昵称：技术成功登录后台。','1482309945','127.0.0.1');/* Mysql backup Separation */
 /* 创建表结构 `yl_article` */
 DROP TABLE IF EXISTS `yl_article`;/* Mysql backup Separation */ CREATE TABLE `yl_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `category_id` int(11) NOT NULL COMMENT '分类ID',
  `title` varchar(100) NOT NULL COMMENT '文章标题',
  `content` text COMMENT '文章内容',
  `click` int(11) NOT NULL DEFAULT '0' COMMENT '点击次数',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_article` */
 INSERT INTO `yl_article` VALUES ('4','1','测试文章','<b>测试呵呵呵呵呵哈哈</b>','1','1','1496911452','1496911797'),('5','4','测试文章2','哈哈哈','1','1','1496911902','1499415474'),('6','0','联系我们','联系我们页面','0','1','1499395819','0'),('7','5','测试文章标题3','<p>测试文章标题3<img src=\"/uploads/images/20170707/5bcac3af899213e95e6ff098d54e8f47.png\" alt=\"undefined\"></p>','1','1','1499415565','0');/* Mysql backup Separation */
 /* 创建表结构 `yl_article_category` */
 DROP TABLE IF EXISTS `yl_article_category`;/* Mysql backup Separation */ CREATE TABLE `yl_article_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(10) NOT NULL COMMENT '分类名称',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `time` int(10) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_article_category` */
 INSERT INTO `yl_article_category` VALUES ('1','最新公告','1','1','1499389224'),('4','常见问题','2','1','1499389271'),('5','帮助中心','3','1','1499389289');/* Mysql backup Separation */
 /* 创建表结构 `yl_auth_group` */
 DROP TABLE IF EXISTS `yl_auth_group`;/* Mysql backup Separation */ CREATE TABLE `yl_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id',
  `title` char(100) NOT NULL DEFAULT '' COMMENT '用户组名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常 0禁用',
  `rules` varchar(255) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_auth_group` */
 INSERT INTO `yl_auth_group` VALUES ('1','超级管理员','1','1,2,3,4,5,6,7,8,9,10,13,14,15,17,19,20,21,22,23,24,25,26,28,31,32,33,35,36,37,38,39,40,41,42'),('2','普通管理员','1','1,2,3,4,5,7,8,9,10,13,14,15,16,17,19,20,21,22,23,24,25,26,28,30,35,36,37,38,39,40,41'),('3','操盘手','1','2,3,4,5,9,14,15,16,17,19,20,21,22,23,24,25,26,35,37,38,39,40,41');/* Mysql backup Separation */
 /* 创建表结构 `yl_auth_group_access` */
 DROP TABLE IF EXISTS `yl_auth_group_access`;/* Mysql backup Separation */ CREATE TABLE `yl_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL COMMENT '用户id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_auth_group_access` */
 INSERT INTO `yl_auth_group_access` VALUES ('1','1'),('2','2');/* Mysql backup Separation */
 /* 创建表结构 `yl_auth_rule` */
 DROP TABLE IF EXISTS `yl_auth_rule`;/* Mysql backup Separation */ CREATE TABLE `yl_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pid` mediumint(8) NOT NULL COMMENT '父id',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则名称',
  `icon` varchar(20) NOT NULL COMMENT 'icon字体',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常 0禁用',
  `condition` char(100) NOT NULL DEFAULT '' COMMENT '规则表达式',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0隐藏 1显示',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_auth_rule` */
 INSERT INTO `yl_auth_rule` VALUES ('1','0','admin/config','配置管理','','1','1','','1','1'),('2','0','admin/user','用户管理','','1','1','','2','1'),('3','0','admin/currency','货币明细','','1','1','','3','1'),('4','0','admin/news','站内消息','','1','1','','4','1'),('5','0','admin/tool','网站工具','','1','1','','7','1'),('6','0','admin/admin','管理员管理','','1','1','','6','1'),('7','1','admin/config/index','基本设置','&#xe620;','1','1','','1','1'),('8','1','admin/config/sms','短信设置','&#xe611;','1','1','','3','1'),('9','1','admin/config/service','客服设置','&#xe60c;','1','1','','0','1'),('10','1','admin/config/level','创客级别','&#xe62c;','1','1','','0','1'),('14','1','admin/config/withdraw','提现设置','&#xe601;','1','1','','0','1'),('13','1','admin/config/payment','支付设置','&#xe609;','1','1','','0','1'),('15','1','admin/config/currency','货币设置','&#xe62b;','1','1','','0','1'),('16','1','admin/config/sign','签到设置','&#xe642;','1','0','','0','0'),('17','1','admin/config/abonus','分红设置','&#xe643;','1','1','','0','1'),('41','3','admin/currency/shopping','购物币明细','&#xe63c;','1','1','','4','1'),('19','2','admin/user/index','用户列表','&#xe612;','1','1','','0','1'),('20','2','admin/user/tree','推荐结构','&#xe636;','1','1','','0','1'),('21','2','admin/user/withdraw','提现记录','&#xe616;','1','1','','0','1'),('22','3','admin/currency/recharge','充值币明细','&#xe63c;','1','1','','0','1'),('23','3','admin/currency/money','提现币明细','&#xe63c;','1','1','','0','1'),('24','3','admin/currency/game','游戏币明细','&#xe63c;','1','1','','3','1'),('25','4','admin/news/index','消息列表','&#xe611;','1','1','','0','1'),('26','4','admin/news/sendmsg','发送消息','&#xe606;','1','1','','0','1'),('38','0','admin/article','文章管理','','1','1','','5','1'),('28','5','admin/tool/backup','备份列表','&#xe61e;','1','1','','0','1'),('39','38','admin/article/index','文章列表','&#xe60a;','1','1','','1','1'),('31','6','admin/admin/index','管理员列表','&#xe612;','1','1','','1','1'),('32','6','admin/admin/group','角色管理','&#xe610;','1','1','','2','1'),('33','6','admin/admin/rule','菜单管理','&#xe61f;','1','1','','3','1'),('35','5','admin/tool/clearcache','清除缓存','&#xe640;','1','1','','0','1'),('36','1','admin/config/email','邮件设置','&#xe63a;','1','1','','2','1'),('37','1','admin/config/nav','导航设置','&#xe62e;','1','1','','0','1'),('40','38','admin/article/category','文章分类','&#xe632;','1','1','','2','1'),('42','1','admin/config/banner','轮播设置','&#xe634;','1','1','','4','1');/* Mysql backup Separation */
 /* 创建表结构 `yl_config` */
 DROP TABLE IF EXISTS `yl_config`;/* Mysql backup Separation */ CREATE TABLE `yl_config` (
  `key` varchar(20) NOT NULL COMMENT '配置名',
  `value` text NOT NULL COMMENT '配置值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_config` */
 INSERT INTO `yl_config` VALUES ('sms','a:4:{s:9:\"smsStatus\";s:1:\"1\";s:9:\"smsLength\";s:1:\"5\";s:7:\"smsUser\";s:10:\"cf_afk2016\";s:7:\"smsPass\";s:8:\"20160318\";}'),('site','a:8:{s:10:\"siteStatus\";s:1:\"1\";s:5:\"deBug\";s:2:\"on\";s:8:\"siteName\";s:8:\"17创客\";s:7:\"siteUrl\";s:14:\"http://tp5.com\";s:8:\"siteLogo\";s:45:\"20170531/b7b273bf6a9e2ed82ee24208dcca63fc.png\";s:6:\"seoKey\";s:8:\"17创客\";s:9:\"recordNum\";s:23:\"粤ICP备123456789号-2\";s:9:\"siteCount\";s:19:\"<script></script>
\";}'),('withdraw','a:9:{s:11:\"minWithdraw\";s:1:\"2\";s:11:\"maxWithdraw\";s:4:\"1000\";s:11:\"withdrawFee\";s:1:\"1\";s:8:\"shopping\";s:2:\"10\";s:11:\"withdrawNum\";s:1:\"1\";s:11:\"withdrawSms\";s:1:\"1\";s:9:\"smsAmount\";s:3:\"100\";s:13:\"withdrawStime\";s:2:\"14\";s:13:\"withdrawEtime\";s:2:\"20\";}'),('service','a:1:{s:7:\"service\";a:2:{i:1;a:4:{s:4:\"name\";s:1:\"1\";s:6:\"mobile\";s:1:\"1\";s:2:\"qq\";s:1:\"1\";s:6:\"wechat\";s:1:\"1\";}i:2;a:4:{s:4:\"name\";s:1:\"3\";s:6:\"mobile\";s:1:\"3\";s:2:\"qq\";s:1:\"3\";s:6:\"wechat\";s:1:\"3\";}}}'),('currency','a:4:{s:8:\"recharge\";s:6:\"金豆\";s:5:\"money\";s:6:\"红豆\";s:8:\"shopping\";s:9:\"购物豆\";s:4:\"game\";s:9:\"游戏豆\";}'),('sign','a:5:{s:8:\"recharge\";a:7:{i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";}s:6:\"abonus\";a:7:{i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";}s:6:\"recast\";a:7:{i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";}s:4:\"game\";a:7:{i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";}s:8:\"shopping\";a:7:{i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";i:5;s:1:\"5\";i:6;s:1:\"6\";i:7;s:1:\"7\";}}'),('email','a:6:{s:4:\"open\";s:1:\"1\";s:4:\"host\";s:11:\"smtp.qq.com\";s:4:\"port\";s:3:\"465\";s:4:\"user\";s:16:\"395486566@qq.com\";s:4:\"pass\";s:16:\"lusbiueuqvodbgjd\";s:4:\"from\";s:9:\"395486566\";}'),('nav','a:1:{s:3:\"nav\";a:7:{i:0;a:4:{s:4:\"sort\";s:1:\"1\";s:4:\"name\";s:6:\"首页\";s:4:\"link\";s:17:\"index/index/index\";s:4:\"show\";s:1:\"1\";}i:1;a:4:{s:4:\"sort\";s:1:\"2\";s:4:\"name\";s:12:\"会员中心\";s:4:\"link\";s:16:\"index/user/index\";s:4:\"show\";s:1:\"1\";}i:2;a:4:{s:4:\"sort\";s:1:\"3\";s:4:\"name\";s:12:\"我的帐户\";s:4:\"link\";s:19:\"index/account/index\";s:4:\"show\";s:1:\"1\";}i:3;a:4:{s:4:\"sort\";s:1:\"4\";s:4:\"name\";s:12:\"复投帐户\";s:4:\"link\";s:17:\"index/account/son\";s:4:\"show\";s:1:\"1\";}i:4;a:4:{s:4:\"sort\";s:1:\"5\";s:4:\"name\";s:12:\"帮助中心\";s:4:\"link\";s:24:\"index/article/index?id=5\";s:4:\"show\";s:1:\"1\";}i:5;a:4:{s:4:\"sort\";s:1:\"6\";s:4:\"name\";s:12:\"常见问题\";s:4:\"link\";s:24:\"index/article/index?id=4\";s:4:\"show\";s:1:\"1\";}i:6;a:4:{s:4:\"sort\";s:1:\"7\";s:4:\"name\";s:12:\"联系我们\";s:4:\"link\";s:34:\"index/article/details?id=6&index=8\";s:4:\"show\";s:1:\"1\";}}}'),('level','a:9:{s:7:\"level_9\";s:2:\"90\";s:7:\"level_8\";s:2:\"80\";s:7:\"level_7\";s:2:\"70\";s:7:\"level_6\";s:2:\"60\";s:7:\"level_5\";s:2:\"50\";s:7:\"level_4\";s:2:\"40\";s:7:\"level_3\";s:2:\"30\";s:7:\"level_2\";s:2:\"20\";s:7:\"level_1\";s:2:\"10\";}'),('abonus','a:17:{s:8:\"main_cap\";s:2:\"17\";s:10:\"main_price\";s:2:\"69\";s:8:\"main_out\";s:3:\"100\";s:11:\"main_invite\";s:2:\"15\";s:12:\"main_charity\";s:2:\"18\";s:11:\"main_abonus\";s:2:\"36\";s:10:\"son_switch\";s:1:\"1\";s:9:\"son_money\";s:2:\"10\";s:7:\"son_cap\";s:3:\"500\";s:9:\"son_price\";s:2:\"69\";s:7:\"son_out\";s:3:\"100\";s:10:\"son_invite\";s:1:\"5\";s:11:\"son_charity\";s:2:\"18\";s:10:\"son_abonus\";s:2:\"36\";s:9:\"is_abonus\";s:1:\"1\";s:8:\"min_rand\";s:3:\"0.3\";s:8:\"max_rand\";s:1:\"1\";}'),('banner','a:5:{i:0;a:2:{s:3:\"src\";s:61:\"/uploads/images/20170707/ac9b2b2703c3b0b6975f07a9b9a55bf2.png\";s:4:\"link\";s:1:\"/\";}i:1;a:2:{s:3:\"src\";s:61:\"/uploads/images/20170707/8fb3004f650f28294af07f5fc53932c1.png\";s:4:\"link\";s:1:\"/\";}i:2;a:2:{s:3:\"src\";s:61:\"/uploads/images/20170707/eb69e0340441056f440d5a0a0c6139b9.png\";s:4:\"link\";s:1:\"/\";}i:3;a:2:{s:3:\"src\";s:61:\"/uploads/images/20170707/5ceee706e2c4a134224f76b31ca36942.png\";s:4:\"link\";s:1:\"/\";}i:4;a:2:{s:3:\"src\";s:0:\"\";s:4:\"link\";s:0:\"\";}}');/* Mysql backup Separation */
 /* 创建表结构 `yl_fund` */
 DROP TABLE IF EXISTS `yl_fund`;/* Mysql backup Separation */ CREATE TABLE `yl_fund` (
  `charity` decimal(10,2) NOT NULL COMMENT '慈善基金',
  `abonus` decimal(10,2) NOT NULL COMMENT '分红基金'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_fund` */
 INSERT INTO `yl_fund` VALUES ('100.00','150.66');/* Mysql backup Separation */
 /* 创建表结构 `yl_fund_record` */
 DROP TABLE IF EXISTS `yl_fund_record`;/* Mysql backup Separation */ CREATE TABLE `yl_fund_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '基金使用记录ID',
  `reason` varchar(255) NOT NULL COMMENT '使用原因',
  `amount` decimal(10,2) NOT NULL COMMENT '使用金额',
  `type` varchar(20) NOT NULL DEFAULT 'abonus' COMMENT '基金类型',
  `time` int(11) NOT NULL COMMENT '使用时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_fund_record` */
 INSERT INTO `yl_fund_record` VALUES ('36','2017-07-15子账户每日分红支出1.23元','1.23','abonus','1500090015'),('37','2017-07-17主账户每日分红支出4.72元','4.72','abonus','1500252577'),('38','2017-07-17子账户每日分红支出1.77元','1.77','abonus','1500252577'),('39','2017-07-19主账户每日分红支出6.56元','6.56','abonus','1500436868'),('40','2017-07-19子账户每日分红支出2.46元','2.46','abonus','1500436868'),('41','2017-07-20主账户每日分红支出7.36元','7.36','abonus','1500513536'),('42','2017-07-20子账户每日分红支出2.76元','2.76','abonus','1500513536'),('43','2017-07-27主账户每日分红支出4.08元','4.08','abonus','1501144777'),('44','2017-07-27子账户每日分红支出1.53元','1.53','abonus','1501144777'),('45','2017-07-28主账户每日分红支出5.2元','5.20','abonus','1501211025'),('46','2017-07-28子账户每日分红支出1.95元','1.95','abonus','1501211026'),('47','2017-07-29主账户每日分红支出1.6元','1.60','abonus','1501291354'),('48','2017-07-29子账户每日分红支出0.60000000000001元','0.60','abonus','1501291354'),('49','2017-07-29主账户每日分红支出0.5元','0.50','abonus','1501291811'),('33','2017-07-14主账户每日分红支出2.72元','2.72','abonus','1500023551'),('34','2017-07-14子账户每日分红支出1.02元','1.02','abonus','1500023551'),('35','2017-07-15主账户每日分红支出3.28元','3.28','abonus','1500090014');/* Mysql backup Separation */
 /* 创建表结构 `yl_main_account` */
 DROP TABLE IF EXISTS `yl_main_account`;/* Mysql backup Separation */ CREATE TABLE `yl_main_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '帐号ID',
  `name` varchar(20) NOT NULL COMMENT '主账户名称',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `price` decimal(10,2) NOT NULL COMMENT '单价',
  `day` int(11) NOT NULL DEFAULT '0' COMMENT '分红天数',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '每日分红',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '累计分红',
  `time` int(11) NOT NULL COMMENT '激活时间',
  `last_time` int(11) NOT NULL COMMENT '最后分红时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 1激活 2出局',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_main_account` */
 INSERT INTO `yl_main_account` VALUES ('1','root-1','1','69.00','9','0.30','100.00','1499844347','1501291811','2'),('2','root-2','1','69.00','9','0.20','100.00','1499844347','1501291811','2'),('3','root-3','1','69.00','0','0.00','0.00','0','0','0'),('4','root-4','1','69.00','0','0.00','0.00','0','0','0'),('5','root-5','1','69.00','0','0.00','0.00','0','0','0'),('6','root-6','1','69.00','0','0.00','0.00','0','0','0'),('7','root-7','1','69.00','0','0.00','0.00','0','0','0'),('8','root-8','1','69.00','0','0.00','0.00','0','0','0'),('9','root-9','1','69.00','0','0.00','0.00','0','0','0'),('10','root-10','1','69.00','0','0.00','0.00','0','0','0'),('11','root-11','1','69.00','0','0.00','0.00','0','0','0'),('12','root-12','1','69.00','0','0.00','0.00','0','0','0'),('13','root-13','1','69.00','0','0.00','0.00','0','0','0'),('14','root-14','1','69.00','0','0.00','0.00','0','0','0'),('15','root-15','1','69.00','0','0.00','0.00','0','0','0'),('16','root-16','1','69.00','0','0.00','0.00','0','0','0'),('17','root-17','1','69.00','0','0.00','0.00','0','0','0'),('18','hello-1','2','69.00','8','0.20','4.44','1499844347','1501291354','1'),('19','hello-2','2','69.00','8','0.20','4.44','1499844347','1501291354','1'),('20','hello-3','2','69.00','8','0.20','4.44','1499844347','1501291354','1'),('21','hello-4','2','69.00','8','0.20','4.44','1499844347','1501291354','1'),('22','hello-5','2','69.00','8','0.20','4.44','1499844347','1501291354','1'),('23','hello-6','2','69.00','8','0.20','4.44','1499845442','1501291354','1'),('24','hello-7','2','69.00','0','0.00','0.00','0','0','0'),('25','hello-8','2','69.00','0','0.00','0.00','0','0','0'),('26','hello-9','2','69.00','0','0.00','0.00','0','0','0'),('27','hello-10','2','69.00','0','0.00','0.00','0','0','0'),('28','hello-11','2','69.00','0','0.00','0.00','0','0','0'),('29','hello-12','2','69.00','0','0.00','0.00','0','0','0'),('30','hello-13','2','69.00','0','0.00','0.00','0','0','0'),('31','hello-14','2','69.00','0','0.00','0.00','0','0','0'),('32','hello-15','2','69.00','0','0.00','0.00','0','0','0'),('33','hello-16','2','69.00','0','0.00','0.00','0','0','0'),('34','hello-17','2','69.00','0','0.00','0.00','0','0','0'),('35','root-1','1','69.00','0','0.00','0.00','0','0','0'),('36','root-1','1','69.00','0','0.00','0.00','0','0','0'),('37','root-2','1','69.00','0','0.00','0.00','0','0','0');/* Mysql backup Separation */
 /* 创建表结构 `yl_money_record` */
 DROP TABLE IF EXISTS `yl_money_record`;/* Mysql backup Separation */ CREATE TABLE `yl_money_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分红ID',
  `user_id` int(11) NOT NULL COMMENT '会员ID',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `currency` varchar(20) NOT NULL COMMENT '货币类型',
  `money` decimal(10,2) NOT NULL COMMENT '金额',
  `message` varchar(100) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '分红类型 1激活 2直推 3复投',
  `time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_money_record` */
 INSERT INTO `yl_money_record` VALUES ('1','2','hello','recharge','-69.00','激活账户【hello-1】扣除69.00金豆','1','1499844347'),('2','1','root','money','15.00','推荐用户hello激活主账户【hello-1】直推奖励','2','1499844347'),('3','2','hello','recharge','-69.00','激活账户【hello-2】扣除69.00金豆','1','1499845034'),('4','1','root','money','15.00','推荐用户hello激活主账户【hello-2】直推奖励','2','1499845034'),('5','2','hello','recharge','-69.00','激活账户【hello-3】扣除69.00金豆','1','1499845099'),('6','1','root','money','15.00','推荐用户hello激活主账户【hello-3】直推奖励','2','1499845099'),('7','2','hello','recharge','-69.00','激活账户【hello-4】扣除69.00金豆','1','1499845179'),('8','1','root','money','15.00','推荐用户hello激活主账户【hello-4】直推奖励','2','1499845179'),('9','2','hello','recharge','-69.00','激活账户【hello-5】扣除69.00金豆','1','1499845317'),('10','1','root','money','15.00','推荐用户hello激活主账户【hello-5】直推奖励','2','1499845317'),('11','2','hello','recharge','-69.00','激活账户【hello-6】扣除69.00金豆','1','1499845442'),('12','1','root','money','15.00','推荐用户hello激活主账户【hello-6】直推奖励','2','1499845442'),('13','2','hello','recharge','-138.00','购买2个复投账户，扣除138金豆','1','1499851270'),('14','2','hello','money','20.00','购买2个复投账户奖励20','3','1499851270'),('15','1','root','money','10.00','推荐用户 hello 购买2个复投账户直推奖励','2','1499851271'),('16','2','hello','recharge','-69.00','购买1个复投账户，扣除69金豆','1','1499851468'),('17','2','hello','money','10.00','购买1个复投账户奖励10','3','1499851468'),('18','1','root','money','5.00','推荐用户 hello 购买1个复投账户直推奖励','2','1499851468'),('81','1','root-1','money','0.34','我的账户【root-1】每日分红奖励0.34','4','1500023550'),('82','1','root-2','money','0.34','我的账户【root-2】每日分红奖励0.34','4','1500023551'),('83','2','hello-1','money','0.34','我的账户【hello-1】每日分红奖励0.34','4','1500023551'),('84','2','hello-2','money','0.34','我的账户【hello-2】每日分红奖励0.34','4','1500023551'),('85','2','hello-3','money','0.34','我的账户【hello-3】每日分红奖励0.34','4','1500023551'),('86','2','hello-4','money','0.34','我的账户【hello-4】每日分红奖励0.34','4','1500023551'),('87','2','hello-5','money','0.34','我的账户【hello-5】每日分红奖励0.34','4','1500023551'),('88','2','hello-6','money','0.34','我的账户【hello-6】每日分红奖励0.34','4','1500023551'),('89','2','hello','money','0.68','复投账户【hello】每日分红奖励0.68','4','1500023551'),('90','2','hello','money','0.34','复投账户【hello】每日分红奖励0.34','4','1500023551'),('91','1','root-1','money','0.41','我的账户【root-1】每日分红奖励0.41','4','1500090014'),('92','1','root-2','money','0.41','我的账户【root-2】每日分红奖励0.41','4','1500090014'),('93','2','hello-1','money','0.41','我的账户【hello-1】每日分红奖励0.41','4','1500090014'),('94','2','hello-2','money','0.41','我的账户【hello-2】每日分红奖励0.41','4','1500090014'),('95','2','hello-3','money','0.41','我的账户【hello-3】每日分红奖励0.41','4','1500090014'),('96','2','hello-4','money','0.41','我的账户【hello-4】每日分红奖励0.41','4','1500090014'),('97','2','hello-5','money','0.41','我的账户【hello-5】每日分红奖励0.41','4','1500090014'),('98','2','hello-6','money','0.41','我的账户【hello-6】每日分红奖励0.41','4','1500090014'),('99','2','hello','money','0.82','复投账户【hello】每日分红奖励0.82','4','1500090015'),('100','2','hello','money','0.41','复投账户【hello】每日分红奖励0.41','4','1500090015'),('101','1','root-1','money','0.59','我的账户【root-1】每日分红奖励0.59','4','1500252576'),('102','1','root-2','money','0.59','我的账户【root-2】每日分红奖励0.59','4','1500252577'),('103','2','hello-1','money','0.59','我的账户【hello-1】每日分红奖励0.59','4','1500252577'),('104','2','hello-2','money','0.59','我的账户【hello-2】每日分红奖励0.59','4','1500252577'),('105','2','hello-3','money','0.59','我的账户【hello-3】每日分红奖励0.59','4','1500252577'),('106','2','hello-4','money','0.59','我的账户【hello-4】每日分红奖励0.59','4','1500252577'),('107','2','hello-5','money','0.59','我的账户【hello-5】每日分红奖励0.59','4','1500252577'),('108','2','hello-6','money','0.59','我的账户【hello-6】每日分红奖励0.59','4','1500252577'),('109','2','hello','money','1.18','复投账户【hello】每日分红奖励1.18','4','1500252577'),('110','2','hello','money','0.59','复投账户【hello】每日分红奖励0.59','4','1500252577'),('111','1','root-1','money','0.82','我的账户【root-1】每日分红奖励0.82','4','1500436867'),('112','1','root-2','money','0.82','我的账户【root-2】每日分红奖励0.82','4','1500436867');/* Mysql backup Separation */
 /* 插入数据 `yl_money_record` */
 INSERT INTO `yl_money_record` VALUES ('113','2','hello-1','money','0.82','我的账户【hello-1】每日分红奖励0.82','4','1500436868');/* Mysql backup Separation */
 /* 插入数据 `yl_money_record` */
 INSERT INTO `yl_money_record` VALUES ('114','2','hello-2','money','0.82','我的账户【hello-2】每日分红奖励0.82','4','1500436868'),('115','2','hello-3','money','0.82','我的账户【hello-3】每日分红奖励0.82','4','1500436868'),('116','2','hello-4','money','0.82','我的账户【hello-4】每日分红奖励0.82','4','1500436868'),('117','2','hello-5','money','0.82','我的账户【hello-5】每日分红奖励0.82','4','1500436868'),('118','2','hello-6','money','0.82','我的账户【hello-6】每日分红奖励0.82','4','1500436868'),('119','2','hello','money','1.64','复投账户【hello】每日分红奖励1.64','4','1500436868'),('120','2','hello','money','0.82','复投账户【hello】每日分红奖励0.82','4','1500436868'),('121','1','root-1','money','0.92','我的账户【root-1】每日分红奖励0.92','4','1500513535'),('122','1','root-2','money','0.92','我的账户【root-2】每日分红奖励0.92','4','1500513536'),('123','2','hello-1','money','0.92','我的账户【hello-1】每日分红奖励0.92','4','1500513536'),('124','2','hello-2','money','0.92','我的账户【hello-2】每日分红奖励0.92','4','1500513536'),('125','2','hello-3','money','0.92','我的账户【hello-3】每日分红奖励0.92','4','1500513536'),('126','2','hello-4','money','0.92','我的账户【hello-4】每日分红奖励0.92','4','1500513536'),('127','2','hello-5','money','0.92','我的账户【hello-5】每日分红奖励0.92','4','1500513536'),('128','2','hello-6','money','0.92','我的账户【hello-6】每日分红奖励0.92','4','1500513536'),('129','2','hello','money','1.84','复投账户【hello】每日分红奖励1.84','4','1500513536'),('130','2','hello','money','0.92','复投账户【hello】每日分红奖励0.92','4','1500513536'),('131','1','root','recharge','-1.00','为 hello 充值 1.00 金豆','5','1500524844'),('132','2','hello','recharge','1.00','充值订单 1 充值到账 1.00 金豆','6','1500524844'),('133','1','root-1','money','0.51','我的账户【root-1】每日分红奖励0.51','4','1501144776'),('134','1','root-2','money','0.51','我的账户【root-2】每日分红奖励0.51','4','1501144776'),('135','2','hello-1','money','0.51','我的账户【hello-1】每日分红奖励0.51','4','1501144776'),('136','2','hello-2','money','0.51','我的账户【hello-2】每日分红奖励0.51','4','1501144777'),('137','2','hello-3','money','0.51','我的账户【hello-3】每日分红奖励0.51','4','1501144777'),('138','2','hello-4','money','0.51','我的账户【hello-4】每日分红奖励0.51','4','1501144777'),('139','2','hello-5','money','0.51','我的账户【hello-5】每日分红奖励0.51','4','1501144777'),('140','2','hello-6','money','0.51','我的账户【hello-6】每日分红奖励0.51','4','1501144777'),('141','2','hello','money','1.02','复投账户【hello】每日分红奖励1.02','4','1501144777'),('142','2','hello','money','0.51','复投账户【hello】每日分红奖励0.51','4','1501144777'),('143','1','root-1','money','0.65','我的账户【root-1】每日分红奖励0.65','4','1501211025'),('144','1','root-2','money','0.65','我的账户【root-2】每日分红奖励0.65','4','1501211025'),('145','2','hello-1','money','0.65','我的账户【hello-1】每日分红奖励0.65','4','1501211025'),('146','2','hello-2','money','0.65','我的账户【hello-2】每日分红奖励0.65','4','1501211025'),('147','2','hello-3','money','0.65','我的账户【hello-3】每日分红奖励0.65','4','1501211025'),('148','2','hello-4','money','0.65','我的账户【hello-4】每日分红奖励0.65','4','1501211025'),('149','2','hello-5','money','0.65','我的账户【hello-5】每日分红奖励0.65','4','1501211025'),('150','2','hello-6','money','0.65','我的账户【hello-6】每日分红奖励0.65','4','1501211025'),('151','2','hello','money','1.30','复投账户【hello】每日分红奖励1.30','4','1501211026'),('152','2','hello','money','0.65','复投账户【hello】每日分红奖励0.65','4','1501211026'),('153','1','root','money','-2.00','申请提现支出 2 元','7','1501223754'),('154','1','root','money','-2.00','申请提现支出 2 元','7','1501223908'),('155','1','root','money','-2.00','申请提现支出 2 元','7','1501224123'),('156','1','root','money','-10.00','申请提现支出 10 元','7','1501225088'),('157','1','root','recharge','-1.00','即时转账给 root 1 游戏豆','8','1501231485'),('158','1','root','game','1.00','root 即时转账收入 1 游戏豆','8','1501231485'),('159','1','root','recharge','-2.00','即时转账给 root 2 购物豆','8','1501231516'),('160','1','root','shopping','2.00','root 即时转账收入 2 购物豆','8','1501231516'),('161','1','root','money','-1.00','即时转账给 root 1 金豆','8','1501231552'),('162','1','root','shopping','1.00','root 即时转账收入 1 金豆','8','1501231553'),('163','1','root','money','-1.00','即时转账给 root 1 金豆','8','1501231643'),('164','1','root','recharge','1.00','root 即时转账收入 1 金豆','8','1501231643'),('165','1','root','money','-2.00','即时转账给 root 2 游戏豆','8','1501231662');/* Mysql backup Separation */
 /* 插入数据 `yl_money_record` */
 INSERT INTO `yl_money_record` VALUES ('166','1','root','game','2.00','root 即时转账收入 2 游戏豆','8','1501231663');/* Mysql backup Separation */
 /* 插入数据 `yl_money_record` */
 INSERT INTO `yl_money_record` VALUES ('167','1','root','money','-3.00','即时转账给 root 3 购物豆','8','1501231681'),('168','1','root','shopping','3.00','root 即时转账收入 3 购物豆','8','1501231681'),('169','1','root','recharge','-1.00','即时转账给 hello 1 金豆','8','1501231814'),('170','2','hello','recharge','1.00','root 即时转账收入 1 金豆','8','1501231815'),('171','1','root','recharge','-1.00','即时转账给 hello 1 游戏豆','8','1501231843'),('172','2','hello','game','1.00','root 即时转账收入 1 游戏豆','8','1501231843'),('173','1','root','recharge','-1.00','即时转账给 hello 1 购物豆','8','1501231860'),('174','2','hello','shopping','1.00','root 即时转账收入 1 购物豆','8','1501231860'),('175','1','root','money','-1.00','即时转账给 hello 1 金豆','8','1501231883'),('176','2','hello','recharge','1.00','root 即时转账收入 1 金豆','8','1501231883'),('177','1','root','money','-1.00','即时转账给 hello 1 游戏豆','8','1501231900'),('178','2','hello','game','1.00','root 即时转账收入 1 游戏豆','8','1501231900'),('179','1','root','money','-1.00','即时转账给 hello 1 购物豆','8','1501231949'),('180','2','hello','shopping','1.00','root 即时转账收入 1 购物豆','8','1501231949'),('181','1','root','recharge','1.00','管理员给用户 root 充值 1 金豆','9','1501234915'),('182','1','root','money','1.00','管理员增加 1 红豆','9','1501234986'),('183','1','root','game','1.00','管理员增加 1 游戏豆','9','1501234994'),('184','1','root','shopping','1.00','管理员增加 1 购物豆','9','1501235001'),('185','1','root','recharge','-1.00','管理员减少 1 金豆','9','1501235020'),('186','1','root','money','-1.00','管理员减少 1 红豆','9','1501235026'),('187','1','root','game','-1.00','管理员减少 1 游戏豆','9','1501235033'),('188','1','root','shopping','-1.00','管理员减少 1 购物豆','9','1501235040'),('189','1','root-1','money','0.20','我的账户【root-1】每日分红奖励0.2','4','1501291353'),('190','1','root-2','money','0.20','我的账户【root-2】每日分红奖励0.2','4','1501291353'),('191','2','hello-1','money','0.20','我的账户【hello-1】每日分红奖励0.2','4','1501291354'),('192','2','hello-2','money','0.20','我的账户【hello-2】每日分红奖励0.2','4','1501291354'),('193','2','hello-3','money','0.20','我的账户【hello-3】每日分红奖励0.2','4','1501291354'),('194','2','hello-4','money','0.20','我的账户【hello-4】每日分红奖励0.2','4','1501291354'),('195','2','hello-5','money','0.20','我的账户【hello-5】每日分红奖励0.2','4','1501291354'),('196','2','hello-6','money','0.20','我的账户【hello-6】每日分红奖励0.2','4','1501291354'),('200','1','root-2','money','0.20','我的账户【root-2】每日分红奖励0.2','4','1501291811'),('199','1','root-1','money','0.30','我的账户【root-1】每日分红奖励0.3','4','1501291811'),('201','1','root','shopping','0.20','申请ID： 2 提现成功，购物豆 返还','7','1501293789'),('202','1','root','shopping','0.20','申请ID： 3 提现成功，购物豆 到账','7','1501293941'),('203','1','root','money','2.00','申请ID： 4 提现失败，红豆 退款','7','1501293961');/* Mysql backup Separation */
 /* 创建表结构 `yl_recharge` */
 DROP TABLE IF EXISTS `yl_recharge`;/* Mysql backup Separation */ CREATE TABLE `yl_recharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '充值订单ID',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `agent_id` int(10) unsigned NOT NULL COMMENT '代理ID',
  `num` decimal(10,2) unsigned NOT NULL COMMENT '充值数量',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态 -1失败 0创建 1支付 2成功',
  `explain` varchar(200) DEFAULT NULL COMMENT '说明',
  `create_time` int(10) unsigned NOT NULL COMMENT '时间',
  `confirm_time` int(10) unsigned DEFAULT NULL COMMENT '确认时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_recharge` */
 INSERT INTO `yl_recharge` VALUES ('1','2','hello','1','1.00','1','用户确认支付','1500518451','1500524844'),('2','2','hello','1','1.00','-1','用户取消充值','1500518451',''),('3','2','hello','1','1.00','0','','1500538647',''),('4','2','hello','1','9.00','0','','1500538823',''),('5','2','hello','1','9.00','0','','1500538936',''),('6','2','hello','1','5.00','0','','1500539011','');/* Mysql backup Separation */
 /* 创建表结构 `yl_session` */
 DROP TABLE IF EXISTS `yl_session`;/* Mysql backup Separation */ CREATE TABLE `yl_session` (
  `session_id` char(40) NOT NULL COMMENT 'SESSION键',
  `data` varchar(20000) DEFAULT NULL COMMENT 'SESSION值',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'SESSION更新时间',
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_session` */
 INSERT INTO `yl_session` VALUES ('thinkp46vm6pslhor4vu97r18bq6gb4','','1501298617');/* Mysql backup Separation */
 /* 创建表结构 `yl_son_account` */
 DROP TABLE IF EXISTS `yl_son_account`;/* Mysql backup Separation */ CREATE TABLE `yl_son_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `price` decimal(10,2) NOT NULL COMMENT '单价',
  `num` int(11) NOT NULL COMMENT '购买子账户数量',
  `day` int(11) NOT NULL DEFAULT '0' COMMENT '分红天数',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '每日分红',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '累计分红',
  `time` int(10) NOT NULL COMMENT '购买时间',
  `last_time` int(11) NOT NULL COMMENT '最后分红时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1分红中 2出局',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_son_account` */
 INSERT INTO `yl_son_account` VALUES ('1','hello','2','69.00','2','8','0.40','8.88','1499851271','1501291354','1'),('2','hello','2','69.00','1','8','0.20','4.44','1499851468','1501291354','1');/* Mysql backup Separation */
 /* 创建表结构 `yl_user` */
 DROP TABLE IF EXISTS `yl_user`;/* Mysql backup Separation */ CREATE TABLE `yl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `mobile` char(11) NOT NULL COMMENT '手机号',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '登录密码',
  `password2` char(32) NOT NULL COMMENT '操作密码',
  `nickname` varchar(20) DEFAULT NULL COMMENT '昵称',
  `realname` varchar(10) DEFAULT NULL COMMENT '真实姓名',
  `level` tinyint(1) NOT NULL DEFAULT '10' COMMENT '创客级别',
  `vip` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'VIP级别',
  `rank` tinyint(1) NOT NULL DEFAULT '0' COMMENT '销售级别',
  `invite` int(11) NOT NULL COMMENT '邀请人',
  `money` decimal(10,2) NOT NULL COMMENT '分红提现币',
  `recharge` decimal(10,2) NOT NULL COMMENT '充值币',
  `game` decimal(10,2) NOT NULL COMMENT '游戏币',
  `shopping` decimal(10,2) NOT NULL COMMENT '购物币',
  `identity` char(18) DEFAULT NULL COMMENT '身份证',
  `agent` tinyint(1) NOT NULL DEFAULT '0' COMMENT '代理',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户状态',
  `qq` varchar(15) DEFAULT NULL COMMENT 'qq号',
  `alipay` varchar(100) DEFAULT NULL COMMENT '支付宝',
  `wechat` varchar(100) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL COMMENT '银行',
  `bank_card` varchar(20) DEFAULT NULL COMMENT '银行卡号',
  `create_time` int(11) NOT NULL COMMENT '注册时间',
  `login_time` int(11) NOT NULL COMMENT '最后登录时间',
  `total_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '总收入',
  `invite_money` decimal(10,2) NOT NULL COMMENT '累计邀请收入',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_user` */
 INSERT INTO `yl_user` VALUES ('1','18627565346','395486566@qq.com','root','2c65af026a11dbb36c121fffabdac51a','','技术','阳先生','10','1','0','0','90.38','494.00','3.00','6.40','43042219950424625X','1','1','395486566','18627565346','18627565346','中国银行','xxxx-xxxx-xxxx','1494036793','1501293891','130.78','0.00'),('2','13026033006','test@163.com','hello','aa186e23eb5ef291cd4a9c36fe887625','','测试','刘先生','10','0','0','1','125.55','382.00','2.00','2.00','','0','1','','','','','','1494036793','1501231968','132.55','0.00'),('3','13012345678','546546@qq.com','test','2c65af026a11dbb36c121fffabdac51a','','','','10','0','0','0','0.00','0.00','0.00','0.00','','0','0','','','','','','0','0','0.00','0.00');/* Mysql backup Separation */
 /* 创建表结构 `yl_user_msg` */
 DROP TABLE IF EXISTS `yl_user_msg`;/* Mysql backup Separation */ CREATE TABLE `yl_user_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_user_id` int(11) DEFAULT NULL COMMENT '发信人id',
  `receive_user_id` int(11) DEFAULT NULL COMMENT '收信人id',
  `info` text COMMENT '信息内容',
  `act_ip` char(20) DEFAULT NULL COMMENT '操作ip',
  `is_read` tinyint(1) DEFAULT '0' COMMENT '是否已阅读',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否已放弃或删除',
  `read_time` int(11) DEFAULT NULL COMMENT '阅读时间',
  `add_time` int(11) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户消息表';/* Mysql backup Separation */
 /* 插入数据 `yl_user_msg` */
 INSERT INTO `yl_user_msg` VALUES ('1','0','1','this is info 1','127.0.0.1','0','1','','',''),('2','1','2','this to user_id eq 1','127.0.0.1','1','1','','','1501146004'),('3','0','0','这是一条所有用户都可以看到的消息。','127.0.0.1','0','0','','1496462868',''),('4','0','1','这是一条系统发送给指定用户的消息。','127.0.0.1','0','0','','1496462977','');/* Mysql backup Separation */
 /* 创建表结构 `yl_withdraw` */
 DROP TABLE IF EXISTS `yl_withdraw`;/* Mysql backup Separation */ CREATE TABLE `yl_withdraw` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '提现ID',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `type` varchar(10) NOT NULL DEFAULT '支付宝' COMMENT '提现方式',
  `money` decimal(10,2) unsigned NOT NULL COMMENT '提现金额',
  `fee` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `paid` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '实付',
  `shopping` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '返购物币',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0待支付 1已支付 2提现失败',
  `note` varchar(100) DEFAULT NULL,
  `create_time` int(10) unsigned DEFAULT NULL COMMENT '申请时间',
  `pay_time` int(10) unsigned DEFAULT '0' COMMENT '支付时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;/* Mysql backup Separation */
 /* 插入数据 `yl_withdraw` */
 INSERT INTO `yl_withdraw` VALUES ('2','1','root','alipay','2.00','0.02','1.78','0.20','1','测试','1501223908','1501293789'),('3','1','root','alipay','2.00','0.02','1.78','0.20','1','测试','1501224123','1501293940'),('4','1','root','alipay','2.00','0.02','1.78','0.20','2','测试失败','1501224993','0'),('5','1','root','bank','10.00','0.10','8.91','0.99','0','测试','1501225088','0');/* Mysql backup Separation */