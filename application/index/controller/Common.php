<?php
namespace app\index\controller;

use app\index\Controller;
use sms\Sms;
use think\Session;

class Common extends Controller
{
    //发送短信
    public function sendSms()
    {
        if ($this->request->isPost()) {
            $input = $this->request->Post();

            if($this->verifyToken($input['__token__']) === false){
                return json(['success' => 0, 'msg' => '令牌错误']);
            }

            $result = $this->validate($input, 'User.sendSms');
            if ($result !== true) {
                return json(['success' => 0, 'msg' => $result]);
            }

            $Sms    = new Sms();
            $status = $Sms->sendSms($input['mobile']);
            if ($status === false) {
                return json(['success' => 0, 'msg' => '发送短信失败，请重试']);
            } else {
                return json(['success' => 1, 'msg' => '发送短信成功']);
            }
        } else {
            return json(['success' => 0, 'msg' => '非法请求']);
        }
    }
}
