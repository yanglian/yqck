<?php
namespace app\index\controller;

use app\index\Controller;
use think\Db;

class Index extends Controller
{
    public function index()
    {
    	$articleArr = Db::name('article')->field('content',true)->where('status',1)->order('id desc')->select();
    	$articles = array();
    	foreach ($articleArr as $article) {
    		if($article['category_id']){
    			$articles[$article['category_id']][] = $article;
    		}
    	}
        //轮播图
        $banners = config('banner');
        foreach ($banners as $k => $val) {
            if(empty($val['src'])){
                unset($banners[$k]);
            }
        }

        $this->assign('banners',$banners);
    	$this->assign('articles',$articles);
        return $this->fetch();
    }
}
