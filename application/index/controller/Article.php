<?php
namespace app\index\controller;
use app\index\Controller;
use think\Db;

class Article extends Controller
{
	//文章列表
	public function index($id)
	{
		switch ($id) {
			case '4':$index = 7;break;
			case '5':$index = 8;break;
			default:$index = 3;break;
		}
		
		$where = ['category_id'=>$id,'status'=>1];
		$articles = Db::name('article')->field('content',true)->where($where)->order('id desc')->paginate(10);
		$category = Db::name('article_category')->where('id',$id)->value('name');

		$this->assign('articles',$articles);
		$this->assign('category',$category);
		$this->assign('index',$index);
		return $this->fetch();
	}

	//文章详情
	public function details($id,$index = 3){
		$article = Db::name('article')->find($id);
		$this->assign('article',$article);
		$this->assign('index',$index);
		return $this->fetch();
	}
}