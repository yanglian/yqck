<?php
namespace app\index\controller;

use app\index\Controller;
use think\Db;

class Agent extends Controller
{
    //代理列表
    public function index()
    {
        $where = [];
        if($this->request->isPost()){
            $field = $this->request->post('field');
            if($field){
                $where['username|realname|mobile|qq|wechat'] = ['like','%'.$field.'%'];
                $this->assign('field',$field);
            }
        }
        $agents = Db::name('user')->where('agent','>',0)->where($where)->paginate(1);
        $currency = config('currency')['recharge'];
        $this->assign('currency',$currency);
        $this->assign('agents',$agents);
        $this->setPayPwd($this->request->url(true));
        return $this->fetch();
    }

    //申请代理
    public function apply()
    {
        if($this->request->isPost()){
            $post = $this->request->post();
            if (!captcha_check($post['code'])) {
                return json(['success' => 0, 'msg' => '验证码错误']);
            }
            if($post['username'] != $this->user['username']){
                return json(['success'=>0,'msg'=>'用户名和当前用户不一致']);
            }
            $apply = Db::name('apply_agent')->where(['status'=>0,'user_id'=>$this->user['id']])->find();
            if($apply){
                return json(['success'=>0,'msg'=>'已经是申请状态，请耐心等待审核']);
            }
            $result = Db::name('apply_agent')->insert([
                'user_id' => $this->user['id'],
                'username' => $this->user['username'],
                'realname' => $post['realname'],
                'identity' => $post['identity'],
                'invitename' => $post['invitename'],
                'time' => time()
            ]);
            if($result){
                return json(['success'=>1,'msg'=>'提交申请成功，请耐心等待审核','url'=>'/user']);
            }
            return json(['success'=>0,'msg'=>'提交申请失败，请重试']);
        }else{
            return $this->fetch();
        }
    }

    //用户充值记录
    public function record()
    {
        if($this->request->isPost()){
            $post = $this->request->post();
            if(!isset($post['id']) || !isset($post['status'])){
                return json(['status'=>0,'msg'=>'非法操作']);
            }
            //判断支付密码
            if(!$this->validatePwd($post['pwd'])){
                return json(['status' => 0, 'msg' => '支付密码错误']);
            }
            if($post['status'] == '-1'){
                $result = Db::name('recharge')->update([
                    'id' => $post['id'],
                    'explain' => '用户取消充值',
                    'status' => '-1'
                ]);
                if($result){
                    return json(['status'=>1,'msg'=>'操作成功']);
                }
            }
            if($post['status'] == '1'){
                $result = Db::name('recharge')->update([
                    'id' => $post['id'],
                    'explain' => '用户确认已支付',
                    'status' => '1'
                ]);
                if($result){
                    return json(['status'=>1,'msg'=>'操作成功']);
                }
            }
            return json(['status'=>0,'msg'=>'操作失败']);
        }else{
            $recharges = Db::view('recharge','*')
            ->view('user','username,alipay,realname,qq,mobile,bank,bank_card','user.id=recharge.agent_id')
            ->where('user_id',$this->user['id'])
            ->order('id desc')
            ->paginate(10);
            $this->assign('recharges',$recharges);
            return $this->fetch();
        }
    }

    //充值确认
    public function confirm()
    {
        $currency = config('currency')['recharge'];
        if($this->request->isPost()){
            $post = $this->request->post();
            if(!isset($post['id'])){
                return json(['status'=>0,'msg'=>'非法操作']);
            }
            //充值失败
            if($post['status'] == '-1'){
                $result = Db::name('recharge')->update([
                    'id' => $post['id'],
                    'explain' => $post['explain'],
                    'status' => '-1'
                ]);
                if($result){
                    return json(['status'=>1,'msg'=>'操作成功']);
                }
            }
            //充值成功
            if($post['status'] == '2'){
                $item = Db::name('recharge')->find($post['id']);
                $result = Db::name('recharge')->update([
                    'id' => $post['id'],
                    'status' => '2',
                    'confirm_time' => time()
                ]);
                if($result){
                    if($item['num'] > $this->user['recharge']){
                        return json(['status'=>0,'msg'=>$currency.'不足，请先充值']);
                    }
                    $message = "为 {$item['username']} 充值 {$item['num']} {$currency}";
                    if(dec_money($this->user['id'],$this->user['username'],'recharge',$item['num'],$message,5)){
                        $message = "充值订单 {$item['id']} 充值到账 {$item['num']} {$currency}";
                        inc_money($item['user_id'],$item['username'],'recharge',$item['num'],$message,6);
                        return json(['status'=>1,'msg'=>'操作成功']);
                    }
                }
            }
            return json(['status'=>0,'msg'=>'操作失败']);
        }else{
            if(!$this->user['agent']){
                $this->redirect('index/user/index');
            }
            $list = Db::name('recharge')->where('agent_id',$this->user['id'])->order('id desc')->paginate(10);
            $this->assign('list',$list);
            $this->assign('currency',$currency);
            return $this->fetch();
        }
    }

}
