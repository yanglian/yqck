<?php
namespace app\index\behavior;
use think\Db;
class Register {

    public function run($User){
        $this->addMainAccount($User);
    }

    //添加17个主账户
    private function addMainAccount($User)
    {
    	$config = config('abonus');
    	$accountArr = [];
    	for ($i=1; $i <= $config['main_cap']; $i++) { 
    		$accountArr[] = [
    			'name' => $User->username . '-' .$i,
    			'user_id' => $User->id,
    			'price' => $config['main_price'],
    		];
    	}
    	Db::name('main_account')->insertAll($accountArr);
    }
}