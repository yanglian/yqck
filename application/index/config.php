<?php
//配置文件
return [

	//默认错误跳转对应的模板文件
    'dispatch_error_tmpl'   => 'public/jump',
    //默认成功跳转对应的模板文件
    'dispatch_success_tmpl' => 'public/jump',

	//View配置
    'view_replace_str' => [
        '__STATIC__' => '/static',
        '__LAYUI__'  => '/static/layui',
    ],

    //不需要登录的方法
    'not_login' => [
    	'index/index/index',
    	'index/common/sendsms',
    	'index/user/login',
    	'index/user/register',
    	'index/article/index',
    	'index/article/details'
    ]
];
