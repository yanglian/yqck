<?php
namespace app\index\validate;

use think\Validate;

class User extends Validate
{
    //验证规则
    protected $rule = [
        'username'  => 'require|max:20|alphaDash',
        'nickname'  => 'chsDash',
        'realname'  => 'chs',
        'password'  => 'require|min:6',
        'passwords' => 'require|confirm:password',
        'mobile'    => 'require|mobile',
        'agreement' => 'require',
        'email'     => 'email',
        'invite'    => 'number',
        'qq'        => 'number'
    ];

    //正则验证
    protected $regex = [
        'mobile' => '/^1\d{10}$/',
    ];

    //错误提示
    protected $message = [
        'username.require'   => '用户名不能为空',
        'username.max'       => '用户名长度不能超过20个字符',
        'username.alphaDash' => '用户名必须是字母数字下划线',
        'nickname.chsDash'   => '昵称必须是汉字、字母、数字和下划线',
        'realname.chs'       => '姓名只能是汉字',
        'password.require'   => '密码不能为空',
        'password.min'       => '密码长度不能小于6个字符',
        'passwords.require'  => '确认密码不能为空',
        'passwords.confirm'  => '确认密码和密码不一致',
        'mobile.require'     => '手机号不能为空',
        'mobile.mobile'      => '手机号格式错误',
        'agreement.require'  => '请同意注册协议',
        'email.email'        => '邮箱地址格式错误',
        'invite.number'      => '邀请人ID必须是数字',
        'qq.number'          => 'QQ号必须是数字'
    ];

    //验证场景
    protected $scene = [
        'sendSms' => ['mobile'],
        'addUser' => ['username', 'mobile', 'password','passwords','invite','agreement']
    ];

}
