<?php
namespace app\admin;

use think\auth\Auth;
use think\Db;
use think\Session;

class Controller extends \think\Controller
{
    public function _initialize()
    {
        //$this->is_Login(); //开发阶段关闭登录。
        $this->is_auth();
    }

    public function is_Login($jump = true)
    {
        if (Session::get('admin.login') != 1 || Session::get('admin.mobile') == null) {
            if ($jump == true) {
                $this->redirect('admin/Login/index');
            }
            return false;
        }
        return true;
    }

    public function is_auth()
    {
        $rule = $this->request->module() . '/' . $this->request->controller() . '/' . $this->request->action();
        $Db   = Db::name('auth_rule');
        if (!$Db->where('name', strtolower($rule))->count()) {
            return true; //查不到规则的直接通过
        }
        $Auth     = new \auth\Auth();
        $admin_id = Session::has('admin.id') ? Session::get('admin.id') : 1;
        if (!$Auth->check($rule, $admin_id)) {
            $this->error('没有访问权限！');
        }
    }

}
