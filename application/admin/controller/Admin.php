<?php
namespace app\admin\controller;

use app\admin\Controller;
use think\Cache;
use think\Db;

class Admin extends Controller
{

    //管理员列表
    public function index()
    {
        $admins = Db::name('admin')->select();
        $status = [0 => '禁用', 1 => '正常'];
        $this->assign('status', $status);
        $this->assign('admins', $admins);
        return $this->fetch();
    }

    //删除管理员
    public function deladmin($id)
    {
        if (intval($id) > 0) {
            if (Db::name('admin')->delete($id)) {
                Db::name('auth_group_access')->where('uid', $id)->delete();
                $this->success('删除管理员成功！');
            } else {
                $this->error('删除管理员失败！');
            }
        }
    }

    //添加编辑管理员
    public function admin($id = 0)
    {
        if ($this->request->isPost()) {
            $input = $this->request->post();
            if (!empty($input['pass'])) {
                $input['pass'] = md5($input['pass']);
            } else {
                unset($input['pass']);
            }
            $role_id = $input['role_id'];
            unset($input['role_id']);
            $return = ['status' => 0, 'msg' => '操作失败'];
            if (empty($input['id'])) {
                if (Db::name('admin')->insert($input)) {
                    $uid = Db::name('admin')->getLastInsID();
                    Db::name('auth_group_access')->insert(['uid' => $uid, 'group_id' => $role_id]);
                    $return['status'] = 1;
                    $return['msg']    = "添加管理员成功";
                }
            } else {
                Db::name('admin')->update($input);
                Db::name('auth_group_access')->where('uid', $input['id'])->update(['group_id' => $role_id]);
                $return['status'] = 1;
                $return['msg']    = "修改管理员成功";
            }
            exit(json_encode($return));
        } else {
            $roles = Db::name('auth_group')->where('status', 1)->field('id,title')->select();
            if ($id > 0) {
                $admin = Db::name('admin')->where('id', $id)->find();
                unset($admin['pass']);
                $access = Db::name('auth_group_access')->where('uid', $id)->find();
                if (!is_null($access)) {
                    $admin['role_id'] = $access['group_id'];
                }
            }
            $this->assign('roles', $roles);
            $this->assign('admin', json_encode($admin));
            return $this->fetch();
        }
    }

    //分配权限
    public function auth($id = 0)
    {
        if ($this->request->isPost()) {
            $rule_id = $this->request->post('rule_id/a');
            $role_id = $this->request->post('role_id');
            sort($rule_id);
            if (!empty($role_id)) {
                $rows = Db::name('auth_group')->update(['id' => $role_id, 'rules' => implode(',', $rule_id)]);
                if ($rows > 0) {
                    $this->success('分配权限成功！');
                }
            }
            $this->error('操作失败！');
        } else {
            $id == 0 && $this->error('缺少参数角色id');
            $Admin = new \app\admin\model\Admin;
            $rule  = Db::name('auth_rule')->field('id,pid,title')->select();
            $rule  = $Admin::sonTree($rule);
            $role  = Db::name('auth_group')->where('id', $id)->find();
            $this->assign('rule', $rule);
            $this->assign('role', $role);
            return $this->fetch();
        }
    }

    //用户组 角色列表
    public function group()
    {
        $attr  = ['禁用', '正常'];
        $group = Db::name('auth_group')->select();
        $this->assign('group', $group);
        $this->assign('attr', $attr);
        return $this->fetch();
    }

    //角色
    public function role($id = 0)
    {
        $db = Db::name('auth_group');
        if ($this->request->isPost()) {
            $input  = $this->request->post();
            $return = ['status' => 0, 'msg' => '操作失败'];
            if (empty($input['id'])) {
                if ($db->insert($input)) {
                    $return['status'] = 1;
                    $return['msg']    = "添加成功";
                }
            } else {
                if ($db->update($input)) {
                    $return['status'] = 1;
                    $return['msg']    = "修改成功";
                }
            }
            exit(json_encode($return));
        } else {
            ($id > 0) && $role = $db->find($id);
            $this->assign('role', json_encode($role));
            return $this->fetch();
        }
    }

    //删除角色
    public function delRole($id = 0)
    {
        if ($id != 0) {
            $db = Db::name('auth_group');
            if ($db->delete($id)) {
                //删除该角色对应的用户
                Db::name('auth_group_access')->where('group_id', $id)->delete();
                $this->success('删除角色成功！');
            } else {
                $this->error('删除角色失败！');
            }
        } else {
            $this->error('操作失败！');
        }
    }

    //规则列表 + 菜单
    public function rule()
    {
        $Admin = new \app\admin\model\Admin;
        $menu  = Db::name('auth_rule')->order('sort')->select();
        $menu  = $Admin::menuTree($menu);
        Cache::set('menuTree', $menu, 3600);
        $this->assign('menu', $menu);
        return $this->fetch();
    }

    //编辑菜单
    public function menu($pid = 0, $id = 0)
    {
        $db = Db::name('auth_rule');
        if ($this->request->isPost()) {
            $input         = $this->request->post();
            $input['name'] = strtolower($input['name']);
            $return        = ['status' => 0, 'msg' => '操作失败'];
            if (!empty($input['id'])) {
                if ($db->update($input)) {
                    $return['status'] = 1;
                    $return['msg']    = "修改成功";
                }
            } else {
                if ($db->insert($input)) {
                    $return['status'] = 1;
                    $return['msg']    = "添加成功";
                }
            }
            Cache::rm('menuTree');
            exit(json_encode($return));
        } else {
            $Admin            = new \app\admin\model\Admin;
            $menu             = $db->select();
            $menu             = $Admin::menuTree($menu);
            $row['pid']       = $pid;
            ($id > 0) && $row = $db->find($id);
            $this->assign('menu', $menu);
            $this->assign('row', json_encode($row));
            return $this->fetch();
        }
    }

    //删除菜单
    public function delMenu($id = 0)
    {
        if ($id != 0) {
            $db = Db::name('auth_rule');
            if ($db->where('pid', '=', $id)->count()) {
                $this->error('该菜单下面还有子菜单，请先删除子菜单！');
            }
            if ($db->delete($id)) {
                Cache::rm('menuTree');
                $this->success('删除菜单成功！');
            } else {
                $this->error('删除菜单失败！');
            }
        } else {
            $this->error('操作失败！');
        }
    }

}
