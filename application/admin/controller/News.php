<?php

namespace app\admin\controller;

use app\admin\Controller;
use think\Db;

class News extends Controller
{
    //消息列表
    public function index()
    {
        //查询条件
        $where = array();
        if ($this->request->isPost()) {
            $post = $this->request->post();
            switch ($post['status']) {
                case '1':
                    $where['is_read'] = 1;
                    break;
                case '2':
                    $where['is_read'] = 0;
                    break;
                case '3':
                    $where['is_delete'] = 1;
                    break;
            }
            if (!empty($post['keyword'])) {
                switch ($post['type']) {
                    case '0':
                        $where['info'] = ['like', '%' . $post['keyword'] . '%'];
                        break;
                    case '1':
                        $where['send_user_id'] = $post['keyword'];
                        break;
                    case '2':
                        $where['receive_user_id'] = $post['keyword'];
                        break;
                }
            }
            $this->assign('post', json_encode($post));
        }
        $msgs = Db::name('user_msg')->where($where)->paginate(10);
        $this->assign('msgs', $msgs);
        return $this->fetch();
    }

    //发送消息到用户
    public function sendMsg()
    {
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $db = Db::name('user_msg');
            if (isset($post['open'])) {
                //添加系统消息
                $info = ['send_user_id' => 0, 'receive_user_id' => 0, 'info' => $post['info'], 'act_ip' => $this->request->ip(), 'add_time' => time()];
                if($db->insert($info)){
                    $this->success('发送系统消息成功！',url('admin/News/index'));
                    exit;
                }
            } else {
                if (!empty($post['from'])) {
                    $uids = array_filter(explode('@', $post['from']));
                    $infos = [];
                    foreach ($uids as $uid) {
                        $infos[] = ['send_user_id' => 0, 'receive_user_id' => $uid, 'info' => $post['info'], 'act_ip' => $this->request->ip(), 'add_time' => time()];
                    }
                    if($db->insertAll($infos)){
                        $this->success('发送消息成功！',url('admin/News/index'));
                        exit;
                    }
                }
            }
            $this->error('发送消息失败...');
        } else {
            return $this->fetch();
        }
    }

    public function article()
    {
        return $this->fetch();
    }

    //编辑一条消息
    public function editMsg($id = 0)
    {
        if ($this->request->isPost()) {
            $post   = $this->request->post();
            $result = Db::name('user_msg')->update($post);
            $return = ['status' => 0, 'msg' => '操作失败！'];
            if ($result) {
                $return['status'] = 1;
                $return['msg']    = "修改消息成功！";
            } else {
                $return['msg'] = "修改消息失败...";
            }
            exit(json_encode($return));
        }
        if ($id > 0) {
            $msg = Db::name('user_msg')->find($id);
            $this->assign('msg', json_encode($msg));
            return $this->fetch();
        }

    }

    //删除消息
    public function delMsg($id = 0)
    {
        //删除一条消息
        if ($id > 0) {
            if (Db::name('user_msg')->delete($id)) {
                $this->success('删除成功');
            }
        }
        //批量删除消息
        if ($this->request->isPost()) {
            $ids = $this->request->post('ids/a');
            if (Db::name('user_msg')->delete($ids)) {
                $this->success('批量删除成功');
            }
        }
        $this->success('操作失败！');
    }

}
