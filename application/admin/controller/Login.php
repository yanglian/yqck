<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Session;
use think\Url;

class Login extends Controller
{
    //登录页面
    public function index()
    {
        if ($this->request->isPost()) {
            $db     = Db::name('admin');
            $mobile = $this->request->post('mobile');
            $admin  = $db->where('mobile', $mobile)->find();
            if (is_null($admin)) {
                $return['status'] = 0;
                $return['info']   = "管理员不存在！";
                exit(json_encode($return));
            }
            $Sms = new \sms\Sms();
            if ($Sms->sendSms($mobile) == true) {

                $log = "管理员：" . $admin['mobile'] . " 昵称：" . $admin['nickname'] . "请求登录，验证码：" . Session::get('mobile_code');
                add_admin_log($log);

                $return['status'] = 1;
                $return['info']   = "发送短信成功，请注意查收！";
                exit(json_encode($return));
            } else {
                $return['status'] = 0;
                $return['info']   = "发送短信失败，请重新获取！";
                exit(json_encode($return));
            }
        } else {
            if(Session::get('admin.login') == 1){
              $this->redirect('admin/Login/index');
              exit;
            }
            return $this->fetch();
        }
    }

    //验证登录
    public function check()
    {
        if (Session::get('admin.login') == 1) {
            $return['status'] = 1;
            $return['info']   = "已经是登录状态，请不要重复登录！";
            $return['url']    = Url::build('admin/Index/index');
            exit(json_encode($return));
        }
        if ($this->request->isPost()) {
            $db     = Db::name('admin');
            $mobile = $this->request->post('mobile');
            $vcode  = $this->request->post('vcode');
            $admin  = $db->where('mobile', $mobile)->find();
            if (is_null($admin)) {
                $return['status'] = 0;
                $return['info']   = "管理员不存在！";
                exit(json_encode($return));
            }
            if (Session::get('mobile') == $mobile && Session::get('mobile_code') == $vcode) {

                Session::set('admin.login', 1);
                Session::set('admin.id', $admin['id']);
                Session::set('admin.mobile', $mobile);
                Session::set('admin.nickname', $admin['nickname']);

                $log = "管理员：" . $admin['mobile'] . " 昵称：" . $admin['nickname'] . "成功登录后台。";
                add_admin_log($log);

                Session::delete('mobile');
                Session::delete('mobile_code');

                $return['status'] = 1;
                $return['info']   = "登录成功！";
                $return['url']    = Url::build('admin/Index/index');
                exit(json_encode($return));
            } else {
                $return['status'] = 0;
                $return['info']   = "验证码错误！";
                exit(json_encode($return));
            }
        }
    }

    //退出登录
    public function logout()
    {
        Session::delete('admin');
        Session::clear();
        $this->redirect('admin/Login/index');
    }
}
