<?php

namespace app\admin\controller;

use app\admin\Controller;
use think\Db;

class Currency extends Controller
{
    //充值币明细
    public function recharge()
    {
        if($this->request->isPost()){
            if($this->request->post('username')){
                $where['username'] = $this->request->post('username');
            }
        }
        $records = Db::name('money_record')->where($where)->where('currency','recharge')->order('id DESC')->paginate(10);
        $this->assign('records',$records);
        $this->assign('post', json_encode($this->request->post()));
        return $this->fetch();
    }

    //分红提现币明细
    public function money()
    {
        if($this->request->isPost()){
            if($this->request->post('username')){
                $where['username'] = $this->request->post('username');
            }
        }
        $records = Db::name('money_record')->where($where)->where('currency','money')->order('id DESC')->paginate(10);
        $this->assign('records',$records);
        $this->assign('post', json_encode($this->request->post()));
        return $this->fetch();
    }

    //游戏币明细
    public function game()
    {
        if($this->request->isPost()){
            if($this->request->post('username')){
                $where['username'] = $this->request->post('username');
            }
        }
        $records = Db::name('money_record')->where($where)->where('currency','game')->order('id DESC')->paginate(10);
        $this->assign('records',$records);
        $this->assign('post', json_encode($this->request->post()));
        return $this->fetch();
    }

    //购物币明细
    public function shopping()
    {
        if($this->request->isPost()){
            if($this->request->post('username')){
                $where['username'] = $this->request->post('username');
            }
        }
        $records = Db::name('money_record')->where($where)->where('currency','shopping')->order('id DESC')->paginate(10);
        $this->assign('records',$records);
        $this->assign('post', json_encode($this->request->post()));
        return $this->fetch();
    }

    //删除
    public function del($id = 0)
    {
        //删除一条消息
        if ($id > 0) {
            if (Db::name('money_record')->delete($id)) {
                $this->success('删除成功');
            }
        }
        //批量删除消息
        if ($this->request->isPost()) {
            $ids = $this->request->post('ids/a');
            if (Db::name('money_record')->delete($ids)) {
                $this->success('批量删除成功');
            }
        }
        $this->success('操作失败！');
    }

    //加减币
    public function incDec($user_id)
    {
        $currency = config('currency');
        if($this->request->isPost()){
            $post = $this->request->post();
            if(empty($post['user_id'])){
                return json(['status'=>0,'msg'=>'请输入用户ID']);
            }
            if(empty($post['num'])){
                return json(['status'=>0,'msg'=>'请输入数量']);
            }
            $user = Db::name('user')->find($post['user_id']);
            if(!$user){
                return json(['status'=>0,'msg'=>'用户不存在']);
            }
            $currencys = [
                '0' => 'recharge',
                '1' => 'money',
                '2' => 'game',
                '3' => 'shopping'
            ];
            //加币
            if($post['mode'] == 1){
                $message = "管理员增加 {$post['num']} {$currency[$currencys[$post['currency']]]}";
                $result = inc_money($user['id'],$user['username'],$currencys[$post['currency']],$post['num'],$message,9);
                if($result){
                    return json(['status'=>1,'msg'=>'操作成功']);
                }
            }
            //减币
            if($post['mode'] == 0){
                $message = "管理员减少 {$post['num']} {$currency[$currencys[$post['currency']]]}";
                $result = dec_money($user['id'],$user['username'],$currencys[$post['currency']],$post['num'],$message,9);
                if($result){
                    return json(['status'=>1,'msg'=>'操作成功']);
                }
            }
            return json(['status'=>0,'msg'=>'操作失败']);
        }else{
            $this->assign('user_id',$user_id);
            $this->assign('currency',$currency);
            return $this->fetch();
        }
    }
}
