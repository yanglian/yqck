<?php

namespace app\admin\controller;

use app\admin\Controller;
use think\Config;

class Tool extends Controller
{

    //数据库
    public function backup($action = "", $file = "")
    {
        $class = new \backup\Mysql();
        $class->setDBName(Config::get('database.database'));
        switch ($action) {
            case 'backup':
                $class->backup() == true ? $this->success("备份成功") : $this->error('备份失败');
                break;
            case 'download':
                $class->downloadFile('backup/' . $file);
                break;
            case 'recover':
                $class->recover($file) == true ? $this->success("还原成功") : $this->error('还原失败');
                break;
            case 'delete':
                unlink('backup/' . $file) == true ? $this->success("删除成功") : $this->error('删除失败');
                break;
            default:
                $files   = $class->myScandir();
                $backups = array();
                foreach ($files as $k => $file) {
                    $backups[$k]['name'] = $file;
                    $backups[$k]['time'] = get_file_time($file, 'backup/');
                    $backups[$k]['size'] = get_file_size($file, 'backup/');
                }
                $this->assign('backups', $backups);
                return $this->fetch();
                break;
        }
    }

    //清数据
    public function clearData()
    {
        return $this->fetch();
    }

    //清缓存
    public function clearCache($cls = "")
    {
        if ($this->request->isPost()) {
            $input = $this->request->post();
            if (empty($input)) {
                $this->error('请选择要清除的项！');
            }
            if (isset($input['app'])) {
                delete_dir(CACHE_PATH);
            }
            if (isset($input['tpl'])) {
                delete_dir(TEMP_PATH);
            }
            if (isset($input['log'])) {
                delete_dir(LOG_PATH);
            }
            $this->success("清除缓存成功！");
        } else {
            if ($cls == "all") {
                delete_dir(RUNTIME_PATH);
                $this->success("清除全部缓存成功！");
                exit;
            }
            return $this->fetch();
        }
    }


}
