<?php
namespace app\admin\controller;
use app\admin\Controller;
use think\Db;
class Article extends Controller
{
	//文章列表
	public function index(){
		$where = array();
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if(isset($post['status']) && $post['status'] != 2){
            	$where['article.status'] = $post['status'];
            }
            if (isset($post['category_id']) && $post['category_id'] != 0) {
            	$where['article.category_id'] = $post['category_id'];
            }
            if (!empty($post['keyword'])) {
            	$where['article.title'] = ['like', '%' . $post['keyword'] . '%'];
            }
            $this->assign('post', json_encode($post));
        }
        $articles = Db::view('article','*')
        ->view('article_category','name','article.category_id=article_category.id','left')
        ->where($where)->order('id desc')->paginate(10);
        $this->assign('articles', $articles);

        $categorys = Db::name('article_category')->select();
        $this->assign('categorys', $categorys);
        return $this->fetch();
	}

	//编辑文章
	public function editArticle($id = 0){
		$db = Db::name('article');
		if($this->request->isPost()){
			$input = $this->request->post();
			unset($input['file']);
			$return = ['status' => 0, 'msg' => '操作失败'];
            if (empty($input['id'])) {
            	$input['create_time'] = time();
                if ($db->insert($input)) {
                    $return['status'] = 1;
                    $return['msg']    = "添加文章成功";
                }
            } else {
            	$input['update_time'] = time();
                if ($db->update($input)) {
                    $return['status'] = 1;
                    $return['msg']    = "修改文章成功";
                }
            }
            exit(json_encode($return));
		}else{
			($id > 0) && $article = $db->find($id);
            $this->assign('article', json_encode($article));

			$categorys = Db::name('article_category')->select();
        	$this->assign('categorys', $categorys);
			return $this->fetch();
		}
	}

	//删除文章
	public function delArticle($id = 0){
		//删除一条分类
        if ($id > 0) {
            if (Db::name('article')->delete($id)) {
                $this->success('删除成功');
            }
        }
        //批量删除分类
        if ($this->request->isPost()) {
            $ids = $this->request->post('ids/a');
            if (Db::name('article')->delete($ids)) {
                $this->success('批量删除成功');
            }
        }
        $this->success('操作失败！');
	}
	
	//文章分类
	public function category(){
		$where = array();
        if ($this->request->isPost()) {
            $post = $this->request->post();
            if(isset($post['status']) && $post['status'] != 2){
            	$where['status'] = $post['status'];
            }
            if (!empty($post['keyword'])) {
            	 $where['name'] = ['like', '%' . $post['keyword'] . '%'];
            }
            $this->assign('post', json_encode($post));
        }
        $categorys = Db::name('article_category')->where($where)->order('sort')->paginate(10);
        $this->assign('categorys', $categorys);
        return $this->fetch();
	}

	//编辑文章分类
	public function editCategory($id = 0){
		$db = Db::name('article_category');
		if($this->request->isPost()){
			$input = $this->request->post();
			$input['time'] = time();
			$return = ['status' => 0, 'msg' => '操作失败'];
            if (empty($input['id'])) {
                if ($db->insert($input)) {
                    $return['status'] = 1;
                    $return['msg']    = "添加成功";
                }
            } else {
                if ($db->update($input)) {
                    $return['status'] = 1;
                    $return['msg']    = "修改成功";
                }
            }
            exit(json_encode($return));
		}else{
			($id > 0) && $category = $db->find($id);
            $this->assign('category', json_encode($category));
			return $this->fetch();
		}
	}

	//删除文章分类
	public function delCategory($id = 0){
		//删除一条分类
        if ($id > 0) {
            if (Db::name('article_category')->delete($id)) {
                $this->success('删除成功');
            }
        }
        //批量删除分类
        if ($this->request->isPost()) {
            $ids = $this->request->post('ids/a');
            if (Db::name('article_category')->delete($ids)) {
                $this->success('批量删除成功');
            }
        }
        $this->success('操作失败！');
	}


}
