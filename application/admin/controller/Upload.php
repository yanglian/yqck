<?php
namespace app\admin\controller;

use app\admin\Controller;
use think\Image;

class Upload extends Controller
{
    //上传图片
    public function image($width = 0, $height = 0)
    {
        $file = $this->request->file('image');
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads' . DS . 'images');
        if ($info) {
            if ($width > 0 && $height > 0) {
                $image = Image::open($info->getPathName());
                $image->thumb($width, $height)->save($info->getPathName());
            }
            $return['status'] = 1;
            $return['msg']    = "上传成功！";
            $return['src']    = str_replace("\\", "/", $info->getSaveName());
            exit(json_encode($return));
        }
    }

    //编辑器上传图片
    public function layedit()
    {
        $file = $this->request->file('file');
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads' . DS . 'images');
        if ($info) {
            $return['code'] = 0;
            $return['msg']    = "上传成功！";
            $return['data']['src']    = str_replace("\\", "/", '/uploads/images/'.$info->getSaveName());
            exit(json_encode($return));
        }else{
            $return['code'] = 1;
            $return['msg']    = "上传失败！";
            exit(json_encode($return));
        }
    }

}
