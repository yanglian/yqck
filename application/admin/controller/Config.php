<?php
namespace app\admin\controller;
use app\admin\Controller;
use think\Db;
class Config extends Controller
{

	//根据配置名保存配置
	public function save($key = '')
	{
		if(empty($key))
			$this->error('请求缺少参数！');
		$db = Db::name('config');
		$config = $db->where('key', $key)->find();
		if(is_null($this->request->post()))
			$this->error('没有post数据！');
		if ($this->request->isPost())
		{	
			$input = $this->request->post();
			if($key == 'nav'){
				sort($input['nav']);
			}
			$data['key'] = $key;
			$data['value'] = serialize($input);
			if(is_null($config)){
				$db->insert($data);
			}else {
				$db->where('key',$key)->update($data);
			}
			$this->success("操作成功！");
		}
	}

	//网站设置
	public function index()
	{
		$db = Db::name('config');
		$site = $db->where('key', 'site')->find();
		$this->assign('site',is_null($site) ? "" : json_encode(unserialize($site['value'])));
		return $this->fetch();
	}

	//客服设置
	public function service()
	{
		if ($this->request->isPost()){
			$array = $this->request->post();
			$array['service'][0] = array();
			sort($array['service']);
			unset($array['service'][0]);
			$this->request->post($array);
			$this->save('service');
		}else{
			$db = Db::name('config');
			$service = $db->where('key', 'service')->find();
			$service = is_null($service) ? "" : unserialize($service['value']);
			$this->assign('service',$service['service']);
			return $this->fetch();
		}
	}

	//轮播图设置
	public function banner(){
		$db = Db::name('config');
		$banner = $db->where('key', 'banner')->find();
		$data = unserialize($banner['value']);
		$banner = array();
		foreach ($data as $k => $v) {
			$keys = array_keys($v);
			foreach ($keys as $k1 => $v2) {
				$banner["{$k}[{$v2}]"] = $v[$keys[$k1]];
			}
		}
		$this->assign('banner',is_null($banner) ? "" : json_encode($banner));
		return $this->fetch();
	}

	//微信设置
	public function wechat()
	{
		$db = Db::name('config');
		$wechat = $db->where('key', 'wechat')->find();
		$this->assign('wechat',is_null($wechat) ? "" : json_encode(unserialize($wechat['value'])));
		return $this->fetch();
	}

	//充值设置
	public function payment()
	{
		$db = Db::name('config');
		$payment = $db->where('key', 'payment')->find();
		$this->assign('payment',is_null($payment) ? "" : json_encode(unserialize($payment['value'])));
		return $this->fetch();
	}

	//提现设置
	public function withdraw()
	{
		$db = Db::name('config');
		$withdraw = $db->where('key', 'withdraw')->find();
		$this->assign('withdraw',is_null($withdraw) ? "" : json_encode(unserialize($withdraw['value'])));
		return $this->fetch();
	}

	//货币设置
	public function currency()
	{
		$db = Db::name('config');
		$currency = $db->where('key', 'currency')->find();
		$this->assign('currency',is_null($currency) ? "" : json_encode(unserialize($currency['value'])));
		return $this->fetch();
	}

	//签到设置
	public function sign()
	{
		$db = Db::name('config');
		$sign = $db->where('key', 'sign')->find();
		$this->assign('sign',is_null($sign) ? "" : unserialize($sign['value']));
		return $this->fetch();
	}

	//短信设置
	public function sms()
	{
		$db = Db::name('config');
		$sms = $db->where('key', 'sms')->find();
		$this->assign('sms',is_null($sms) ? "" : json_encode(unserialize($sms['value'])));
		return $this->fetch();
	}

	//邮件设置
	public function email()
	{
		$db = Db::name('config');
		$email = $db->where('key', 'email')->find();
		$this->assign('email',is_null($email) ? "" : json_encode(unserialize($email['value'])));
		return $this->fetch();
	}

	//导航设置
	public function nav()
	{
		$db = Db::name('config');
		$nav = $db->where('key', 'nav')->find();
		$navs = unserialize($nav['value']);
		$this->assign('navs',$navs['nav']);
		return $this->fetch();
	}

	//创客级别设置
	public function level()
	{
		$db = Db::name('config');
		$level = $db->where('key', 'level')->find();
		$this->assign('level',is_null($level) ? "" : json_encode(unserialize($level['value'])));
		return $this->fetch();
	}

	//分红设置
	public function abonus()
	{
		$db = Db::name('config');
		$abonus = $db->where('key', 'abonus')->find();
		$this->assign('abonus',is_null($abonus) ? "" : json_encode(unserialize($abonus['value'])));
		return $this->fetch();
	}

}
