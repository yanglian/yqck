<?php
namespace app\admin\controller;

use app\admin\Controller;
use think\Db;

class Index extends Controller
{
    public function index()
    {
        $statistics = []; //统计集合
        
        $dayStime   = strtotime(date('Y-m-d', time())); //今天开始时间
        $weekStime  = mktime(0, 0, 0, date('m'), date('d') - date('w') + 1 - 7, date('Y')); //本周开始时间
        $monthStime = mktime(0, 0, 0, date('m'), 1, date('Y')); //本月开始时间

        //已发放分红
        $expAbonus               = Db::name('fund_record')->where('type', 'abonus')->sum('amount');
        $statistics['expAbonus'] = $expAbonus;

        //剩余分红基金
        $leftAbonus               = Db::name('fund')->where('1=1')->value('abonus');
        $statistics['leftAbonus'] = $leftAbonus;

        //市场总分红基金
        $totalAbonus               = $expAbonus + $leftAbonus;
        $statistics['totalAbonus'] = $totalAbonus;

        //已使用慈善基金
        $expCharity               = Db::name('fund_record')->where('type', 'charity')->sum('amount');
        $statistics['expCharity'] = $expCharity;

        //剩余慈善基金
        $leftCharity               = Db::name('fund')->where('1=1')->value('charity');
        $statistics['leftCharity'] = $leftCharity;

        //总慈善基金
        $totalCharity               = $expCharity + $leftCharity;
        $statistics['totalCharity'] = $totalCharity;

        //今日注册用户
        $dayRegister               = Db::name('user')->where('create_time', '>', $dayStime)->count();
        $statistics['dayRegister'] = $dayRegister;

        //本周注册用户
        $weekRegister               = Db::name('user')->where('create_time', '>', $weekStime)->count();
        $statistics['weekRegister'] = $weekRegister;

        //本月注册用户
        $monthRegister               = Db::name('user')->where('create_time', '>', $monthStime)->count();
        $statistics['monthRegister'] = $monthRegister;

        //有效会员总数
        $validUser               = Db::name('user')->where('status', 1)->count();
        $statistics['validUser'] = $validUser;

        //会员总数
        $totlaUser               = Db::name('user')->count();
        $statistics['totlaUser'] = $totlaUser;

        //今日激活账户
        $dayActive = Db::name('main_account')->where('time','>',$dayStime)->count();
        $dayActive += Db::name('son_account')->where('time','>',$dayStime)->sum('num');
        $statistics['dayActive'] = $dayActive;

        //本周激活账户
        $weekActive = Db::name('main_account')->where('time','>',$weekStime)->count();
        $weekActive += Db::name('son_account')->where('time','>',$weekStime)->sum('num');
        $statistics['weekActive'] = $weekActive;

        //本月激活账户
        $monthActive = Db::name('main_account')->where('time','>',$monthStime)->count();
        $monthActive += Db::name('son_account')->where('time','>',$monthStime)->sum('num');
        $statistics['monthActive'] = $monthActive;

        //当前激活账户总数
        $currentActive = Db::name('main_account')->where('status',1)->count();
        $currentActive += Db::name('son_account')->where('status',1)->sum('num');
        $statistics['currentActive'] = $currentActive;
        
        //累计激活账户总数
        $totalActive = Db::name('main_account')->where('status','>',0)->count();
        $totalActive += Db::name('son_account')->where('status','>',0)->sum('num');
        $statistics['totalActive'] = $totalActive;

        //今日出局账户
        $dayOut = Db::name('main_account')->where('last_time','>',$dayStime)->where('status',2)->count();
        $dayOut += Db::name('son_account')->where('last_time','>',$dayStime)->where('status',2)->sum('num');
        $statistics['dayOut'] = $dayOut;

        //本周出局账户
        $weekOut = Db::name('main_account')->where('last_time','>',$weekStime)->where('status',2)->count();
        $weekOut += Db::name('son_account')->where('last_time','>',$weekStime)->where('status',2)->sum('num');
        $statistics['weekOut'] = $weekOut;

        //本月出局账户
        $monthOut = Db::name('main_account')->where('last_time','>',$monthStime)->where('status',2)->count();
        $monthOut += Db::name('son_account')->where('last_time','>',$monthStime)->where('status',2)->sum('num');
        $statistics['monthOut'] = $monthOut;

        //累计出局账户总数
        $totalOut = Db::name('main_account')->where('status',2)->count();
        $totalOut += Db::name('son_account')->where('status',2)->sum('num');
        $statistics['totalOut'] = $totalOut;

        //充值币数量
        $totalRecharge = Db::name('money_record')->where('currency','recharge')->sum('money');
        $statistics['totalRecharge'] = $totalRecharge;

        //提现币数量
        $totalMoney = Db::name('money_record')->where('currency','money')->sum('money');
        $statistics['totalMoney'] = $totalMoney;
        
        //游戏币数量
        $totalGame = Db::name('money_record')->where('currency','game')->sum('money');
        $statistics['totalGame'] = $totalGame;
        
        //购物币数量
        $totalShopping = Db::name('money_record')->where('currency','shopping')->sum('money');
        $statistics['totalShopping'] = $totalShopping;
        
        //货币总数量
        $totalCurrency = Db::name('money_record')->sum('money');
        $statistics['totalCurrency'] = $totalCurrency;

        //投资总金额
        $totalInvest = Db::name('main_account')->where('status','>',0)->sum('price');
        $totalInvest += Db::name('son_account')->where('status','>',0)->sum('price*num');
        $statistics['totalInvest'] = $totalInvest;

        //已提现金额
        $totalWithdraw = Db::name('withdraw')->where('status',1)->sum('paid');
        $statistics['totalWithdraw'] = $totalWithdraw;
        $this->assign('s', $statistics);
        return $this->fetch();
    }

}
