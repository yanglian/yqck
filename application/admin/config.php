<?php
error_reporting(E_ERROR | E_PARSE);
//配置文件
return [
    'view_replace_str'      => [
        '__STATIC__' => '/static/admin',
        '__LAYUI__'  => '/static/layui',
    ],
    //默认错误跳转对应的模板文件
    'dispatch_error_tmpl'   => 'layout/jump',
    //默认成功跳转对应的模板文件
    'dispatch_success_tmpl' => 'layout/jump',

    //分页配置
    'paginate'              => [
        'type'      => 'admin',
        'var_page'  => 'page',
        'list_rows' => 15,
    ],

];
