<?php
namespace app\admin\model;

use think\Model;
use think\Cache;

class Admin extends Model
{
    protected $table = 'admin';

    //菜单树
    static public function menuTree($menu, $html = '└─&nbsp;', $pid = 0, $level = 0)
    {
        if(Cache::get('menuTree') != false){
          return Cache::get('menuTree');
        }
        $array = array();
        $status = [0=>'禁用',1=>'正常'];
        foreach ($menu as $k => $v) {
            if ($v['pid'] == $pid) {
                $v['level'] = $level + 1;
                $v['html']  = str_repeat($html, $level);
                $v['status'] = $status[$v['status']];
                $array[] = $v;
                $array = array_merge($array, self::menuTree($menu, $html, $v['id'], $level + 1));
            }
        }
        return $array;
    }

    //子孙树
    public function sonTree($rule)
    {
      $tree = [];
      foreach($rule as $v){
          $tree[$v['id']] = $v;
          $tree[$v['id']]['son'] = array();
      }
      foreach ($tree as $k => $v) {
          if ($v['pid'] > 0) {
              $tree[$v['pid']]['son'][] = &$tree[$k];
              unset($tree[$k]);
          }
      }
      return $tree;
    }
}
