<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;
//设置全局变量规则
Route::pattern([
    'name'  =>  '\w+',
    'id'    =>  '\d+'
]);
//首页路由
Route::rule('/','index/index/index','GET');
//注册路由
Route::rule('register','index/user/register','GET|POST');
//登录路由
Route::rule('login','index/user/login','GET|POST');
//会员中心
Route::rule('user','index/user/index','GET');
//基本信息
Route::rule('user/info','index/user/info','GET|POST');
//邀请好友
Route::rule('user/invite','index/user/invite','GET|POST');
//退出登录
Route::rule('user/logout','index/user/logout','GET');
//用户充值
Route::rule('recharge','index/user/recharge','GET|POST');
//用户提现
Route::rule('withdraw','index/user/withdraw','GET|POST');
//用户提现
Route::rule('myWithdraw','index/user/myWithdraw','GET');
//用户挂售
Route::rule('sell','index/sell/sell','GET|POST');
//挂售记录
Route::rule('mySell','index/sell/mySell','GET');
//用户转账
Route::rule('transfer','index/user/transfer','GET|POST');
//文章列表
Route::rule('article/list/:id','index/article/index','GET');
//文章详情
Route::rule('article/:id','index/article/details','GET');
//我的账户
Route::rule('account','index/account/index','GET|POST');
//复投账户
Route::rule('account/son','index/account/son','GET');
//每日市场
Route::rule('account/daily','index/account/daily','GET');
//账户明细
Route::rule('account/record','index/account/record','GET');
//代理列表
Route::rule('agent','index/agent/index','GET|POST');
//代理充值
Route::rule('agent/recharge','index/agent/recharge','GET|POST');
//充值确认
Route::rule('recharge/confirm','index/agent/confirm','GET|POST');
//充值记录
Route::rule('recharge/record','index/agent/record','GET|POST');
//接单列表
Route::rule('recharge/orders','index/sell/orders','GET');
//我的接单
Route::rule('recharge/myOrders','index/sell/myOrders','GET|POST');